jQuery.noConflict();

(function ($) {

    // Loads extra scripts when all ajax links have resolved
    function loadScripts(numAjaxLinksRemaining) {
        if (numAjaxLinksRemaining <= 0) {
            enquireClick();

            $.ajax({
                url: '/themes/xone/javascript/combined.js?v=2',
                dataType: "script",
                complete: function () {
                    var s = document.createElement("script");
                    s.type = "text/javascript";
                    s.src = "https://www.google.com/recaptcha/api.js";
                    $("head").append(s);

                    window.scrollTo(0, 0);
                    $('#preloader').remove();
                    setupContactForm();

                    $('a').click(function (e) {
                        var href = $(e.target).attr('href');
                        var target = $(e.target).attr('target');

                        if (href && target && href.substr(0, 7) && target == '_blank') {
                            ga('send', {
                                hitType: 'event',
                                eventCategory: 'PortfolioLink',
                                eventAction: href
                            });
                        }
                    });

                    $('.beginGame').click(function (e) {
                        ga('send', {
                            hitType: 'event',
                            eventCategory: 'Game',
                            eventAction: 'Play Spaceship'
                        });
                    });
                },
                cache: true
            });

            // Enable tooltips
            $('[data-toggle="tooltip"]').tooltip();
        }
    }

    $(document).ready(function () {
        window.scrollTo(0, 0);

        // Get ajax content from ajax links
        var numAjaxLinksRemaining = $('a.ajax-link').length;

        loadScripts(numAjaxLinksRemaining);

        $('a.ajax-link').each(function () {
            var ajaxLink = $(this);
            var href = ajaxLink.attr('href');
            var target = ajaxLink.data('target');
            var title = ajaxLink.data('title');
            var targetContainer = $(target);
            var allSimilarLinks = $('a.ajax-link[href="' + href + '"]'); 

            if (!$(this).hasClass('started-ajax')) {
                allSimilarLinks.addClass('started-ajax');

                if (targetContainer.length == 0) {
                    numAjaxLinksRemaining -= allSimilarLinks.length;
                    loadScripts(numAjaxLinksRemaining);
                } else {
                    targetContainer.addClass('ajax-target').data('link', href).data('title', title);

                    $.ajax(href).done(function (response) {
                        targetContainer.html(response);
                        allSimilarLinks.attr('href', target);

                        numAjaxLinksRemaining -= allSimilarLinks.length;
                        loadScripts(numAjaxLinksRemaining);
                    });
                }
            }
        });

        var theView = "";

        // When we scroll to an ajax target, change the url in the address bar
        /*if ($('body.HomePage').length) {
            $(window).scroll(function () {
                if ($('#preloader').length == 0) {
                    var currentView = "";
                    var currentTitle = "";
                    var currentViewPixels = 0;

                    $('.ajax-target').each(function () {
                        if (Utils.isElementInView($(this))) {
                            // TODO: fix this mess
                            if ($(this).offset().top > currentViewPixels) {
                                currentViewPixels = $(this).offset().top;
                                currentView = $(this).data('link');
                                currentTitle = $(this).data('title');
                            }

                            currentViewPixels = $(this).offset().top;
                            //currentViewPixels = amountInView;
                            console.log("Ajax target " + currentView + ", offset top: " + $(this).offset().top);
                        }
                    });

                    if (currentView) {
                        window.history.pushState("", currentTitle, currentView);
                    } else {
                        window.history.pushState("", "Home", "/");
                    }

                    if (currentView !== theView) {
                        theView = currentView;

                        $('a[data-target]').each(function () {
                            var target = $(this).data('target');
                            if (target) {
                                $(this).attr('href', currentView + target);
                            }
                        });
                    }
                }
            });
        }*/

        enquireClick();
    });

    function enquireClick() {
        $('.enquire-now').click(function () {
            var subject = $(this).data('title');
            if ($('textarea[name="message"]').length) {
                $('textarea[name="message"]').val("I'm interested in: " + subject + '\n\n');
            }
        });
    }

    function setupContactForm() {
        $('.xone-contact').submit(function () {
            var $form = $(this);
            var submitData = $form.serialize();
            var $email = $form.find('input[name="email"]');
            var $name = $form.find('input[name="name"]');
            var $message = $form.find('textarea[name="message"]');
            var $submit = $form.find('input[name="submit"]');
            var $dataStatus = $form.find('.data-status');

            $email.attr('disabled', 'disabled');
            $name.attr('disabled', 'disabled');
            $message.attr('disabled', 'disabled');
            $submit.attr('disabled', 'disabled');

            $dataStatus.show().html('<div class="alert alert-info"><strong>Loading...</strong></div>');

            $.ajax({ // Send an offer process with AJAX
                type: 'POST',
                url: '/contact/form',
                data: submitData + '&action=add',
                dataType: 'html',
                success: function (msg) {
                    if (parseInt(msg, 0) !== 0) {
                        if (msg === 'success') {
                            ga('send', {
                                hitType: 'event',
                                eventCategory: 'Contact',
                                eventAction: 'Submitted'
                            });

                            $email.val('').removeAttr('disabled');
                            $name.val('').removeAttr('disabled');
                            $message.val('').removeAttr('disabled');
                            $submit.removeAttr('disabled');
                            $dataStatus.html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><i class="ion-close"></i></button>Thank you! I\'ll be in touch with you shortly.</div>').fadeIn();
                        } else {
                            $email.removeAttr('disabled');
                            $name.removeAttr('disabled');
                            $message.removeAttr('disabled');
                            $submit.removeAttr('disabled');
                            $dataStatus.html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><i class="ion-close"></i></button>Please fill out all the fields.</div>').fadeIn();
                        }
                    }
                }
            });

            return false;
        });
    }

    /*var $page = $('#main');
    var options = {
        debug: true,
        prefetch: true,
        cacheLength: 2,
        forms: 'form',
        onStart: {
            duration: 250, // Duration of our animation
            render: function ($container) {
                // Add your CSS animation reversing class
                $container.addClass('is-exiting');
                // Restart your animation
                smoothState.restartCSSAnimations();
            }
        },
        onReady: {
            duration: 0,
            render: function ($container, $newContent) {
                // Remove your CSS animation reversing class
                $container.removeClass('is-exiting');
                // Inject the new content
                $container.html($newContent);
            }
        }
    };
    var smoothState = $page.smoothState(options).data('smoothState');*/

    function Utils() {

    }

    Utils.prototype = {
        constructor: Utils,
        isElementInView: function (element, fullyInView) {
            var pageTop = $(window).scrollTop();
            var pageBottom = pageTop + $(window).height();
            var elementTop = $(element).offset().top;
            var elementBottom = elementTop + $(element).height();

            if (fullyInView === true) {
                return ((pageTop < elementTop) && (pageBottom > elementBottom));
            } else {
                return ((elementTop <= pageBottom) && (elementBottom >= pageTop));
            }
        },
        getAmountInView: function (element) {
            var pageTop = $(window).scrollTop();
            var elementTop = $(element).offset().top;

            return pageTop - elementTop;
        }
    };

    var Utils = new Utils();

}(jQuery));