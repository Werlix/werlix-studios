<div class="dzsparallaxer auto-init" data-options='' style="height: 350px;">

    <div class="divimage dzsparallaxer--target " style="width: 101%; height: 600px; background-image: url($ThemeDir/assets/images/bg-4.jpg)">
    </div>
    <div class="parallax-text text-center center-it">
        <h2>$Title</h2>
    </div>
</div>

<% if $IsAjax %>
<% else %>
<% include Navigation %>
<% end_if %>

<section>
    <div class="space-100"></div>
    <div class="container text-center">
        $Content
        $Form
    </div>
</section>

<div class="space-70"></div>