﻿module SpaceshipGame {

    export class Game extends Phaser.Game {

        pageHeight: number;

        constructor() {

            super('100%', '100%', Phaser.AUTO, 'game-content', null, true);

            this.setPageHeight();

            this.state.add('Boot', Boot, false);
            this.state.add('Preloader', Preloader, false);
            this.state.add('MainMenu', MainMenu, false);
            this.state.add('Level1', Level1, false);

            this.state.start('Boot');

        }

        getPageHeight() {
            return this.pageHeight;
        }

        setPageHeight() {
            var body: HTMLElement = document.body;
            var html: HTMLElement = document.documentElement;

            this.pageHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
        }

    }

} 