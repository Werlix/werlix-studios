﻿module SpaceshipGame {

    export class Level1 extends Phaser.State {

        background: Phaser.Sprite;
        music: Phaser.Sound;
        player: SpaceshipGame.Player = null;
        game: SpaceshipGame.Game;

        goingToY: number = -1;
        scrollingToY: number = -1;

        hasClicked: boolean = false;
        hasPressedArrows: boolean = false;

        create() {

            this.game.world.setBounds(0, 0, this.game.width, this.game.pageHeight);

            //this.player = new Player(this.game, this.game.world.centerX - 100, 300, 'blue');

            var self = this;

            window.addEventListener('resize', function () {
                // Set game to match window width
                var pageWidth = self.getPageWidth();

                self.game.width = pageWidth;
                self.game.canvas.width = pageWidth;
                self.game.world.setBounds(0, 0, pageWidth, self.game.pageHeight);
                self.game.world.x = 0;
            });

            document.onmousedown = function (e) {
                console.log('onmousedown');
                if (self.player !== null) {
                    self.hasClicked = true;
                    self.player.mouseDownEvent(e);
                }
            }
            document.onmouseup = function (e) {
                console.log('onmouseup');
                if (self.player !== null) {
                    self.player.mouseUpEvent(e);
                }
            }
            document.ontouchstart = function (e) {
                console.log('ontouchstart');
                if (self.player !== null) {
                    self.hasClicked = true;
                    self.player.mouseDownEvent(e);
                }
            }
            document.ontouchend = function (e) {
                console.log('ontouchend');
                if (self.player !== null) {
                    self.player.mouseUpEvent(e);
                }
            }

            // When "goingTo" links are clicked, scroll to that element
            var scrollLinks = document.getElementsByClassName('goingTo');

            for (var i = 0; i < scrollLinks.length; i++) {
                scrollLinks[i].addEventListener('click', function (e) {
                    var target:string = this.getAttribute('data-target');
                    if (target.substr(0, 1) == '#') {
                        target = target.substr(1);
                    }

                    var targetElement = document.getElementById(target);

                    if (targetElement) {
                        if (self.player !== null) {
                            self.goingToY = targetElement.offsetTop + (self.game.height / 2);
                            if (self.goingToY < self.player.y) {
                                self.player.angle = -90;
                            } else {
                                self.player.angle = 90;
                            }
                            self.player.thrusterForceOn = true;
                            self.player.body.acceleration.set(0);
                            self.player.body.velocity.x = 0;
                            self.player.body.velocity.y = 0;
                        } else {
                            self.scrollingToY = targetElement.offsetTop;
                        }

                        e.preventDefault();

                        return false;
                    }
                }, false);
            }

            var beginGameButtons = document.getElementsByClassName('beginGame');

            var beginGameListener: EventListener = function (e) {
                if (self.player == null) {
                    self.game.setPageHeight();
                    self.game.world.setBounds(0, 0, self.game.width, self.game.pageHeight);

                    self.player = new Player(self.game, self.game.world.centerX, document.body.scrollTop + (self.game.height / 2), 'gold', self);
                    self.game.camera.follow(self.player);
                }

                self.startTutorial();
                self.addGameCSS();

                var beginGame1 = document.getElementById('beginGame1');
                var beginGame2 = document.getElementById('beginGame2');

                if (beginGame1) {
                    beginGame1.parentNode.removeChild(beginGame1);
                }
                if (beginGame2) {
                    beginGame2.parentNode.removeChild(beginGame2);
                }
            };

            for (var i = 0; i < beginGameButtons.length; i++) {
                beginGameButtons[i].addEventListener('click', beginGameListener);
            }
        }

        update() {
            if (this.goingToY > -1) {
                if ((this.player.y + 25) < this.goingToY) {
                    this.player.y += 25;
                } else if ((this.player.y - 25) > this.goingToY) {
                    this.player.y -= 25;
                } else {
                    this.goingToY = -1;
                    this.player.thrusterForceOn = false;
                    this.player.thrusterForceOff = true;
                }
            }

            if (this.scrollingToY > -1) {
                //console.log('Scrolling to: ' + this.scrollingToY + ' current scroll: ' + document.documentElement.scrollTop);
                if ((document.documentElement.scrollTop + 40) < this.scrollingToY) {
                    //document.documentElement.scrollTop += 40;
                    window.scrollTo(0, document.documentElement.scrollTop + 40);
                } else if ((document.documentElement.scrollTop - 40) > this.scrollingToY) {
                    //document.documentElement.scrollTop -= 40;
                    window.scrollTo(0, document.documentElement.scrollTop - 40);
                } else {
                    this.scrollingToY = -1;
                }
            }

            if (this.player !== null) {
                window.scrollTo(0, this.game.camera.y);
            }
        }

        startTutorial(): void {
            var self = this;

            setTimeout(function () {
                if (!self.hasClicked || !self.hasPressedArrows) {
                    document.getElementById('gameHint').style.transform = 'scale(1) translateX(-50%) translateY(-200%)';
                    document.getElementById('gameHint').style.display = 'block';
                }

                var interval = setInterval(function () {
                    if (self.hasClicked || self.hasPressedArrows) {
                        document.getElementById('gameHint').style.transform = 'scale(0) translateX(-50%) translateY(-200%)';
                        document.getElementById('gameHint').style.display = 'hidden';
                        clearInterval(interval);
                    }
                }, 3000);

                setTimeout(function () {
                    document.getElementById('gameHint').style.transform = 'scale(0) translateX(-50%) translateY(-200%)';
                    document.getElementById('gameHint').style.display = 'hidden';
                    clearInterval(interval);
                }, 8000);
            }, 6000);
        }

        addGameCSS(): void {
            document.body.style.cssText = 
                "overflow: hidden;" +
                "-webkit-touch-callout: none;" +
                "-webkit-user-select: none;" +
                "-khtml-user-select: none;" +
                "-moz-user-select: none;" +
                "-ms-user-select: none;" +
                "user-select: none;" +
                "cursor: url('/game-assets/cursor.cur'), auto;";
        }

        getPageWidth(): number {
            var body: HTMLElement = document.body;
            var html: HTMLElement = document.documentElement;

            return Math.max(body.scrollWidth, body.offsetWidth, html.clientWidth, html.scrollWidth, html.offsetWidth);
        }

        getCoords(elem): Phaser.Point {
            var box = elem.getBoundingClientRect();

            var body = document.body;
            var docEl = document.documentElement;

            var scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
            var scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft;

            var clientTop = docEl.clientTop || body.clientTop || 0;
            var clientLeft = docEl.clientLeft || body.clientLeft || 0;

            var top = box.top + scrollTop - clientTop;
            var left = box.left + scrollLeft - clientLeft;

            return new Phaser.Point(left, top);
        }

    }
} 