﻿module SpaceshipGame {

    export class Preloader extends Phaser.State {

        preloadBar: Phaser.Sprite;

        preload() {

            //  Set-up our preloader sprite
            //this.preloadBar = this.add.sprite(200, 250, 'preloadBar');
            //this.load.setPreloadSprite(this.preloadBar);

            //  Load our actual games assets
            this.load.spritesheet('goldship', 'game-assets/goldship.png', 212, 164, 9);
            this.load.spritesheet('purpleship', 'game-assets/purpleship.png', 164, 212, 9);
            this.load.spritesheet('goldthruster', 'game-assets/thrustergold.png', 181, 153);
            this.load.spritesheet('bluethruster', 'game-assets/thrusterbluesm.png', 181, 181);
            this.load.spritesheet('laser', 'game-assets/laser2.png', 17, 33);
        }

        create() {

            //var tween = this.add.tween(this.preloadBar).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true);
            //tween.onComplete.add(this.startGame, this);

            //document.getElementById('preloader').remove();

            console.log('starting');

            this.startGame();

        }

        startMainMenu() {

            this.game.state.start('MainMenu', true, false);

        }

        startGame() {

            this.game.state.start('Level1', true, false);



        }

    }

}