﻿module SpaceshipGame {

    const DRAG: number = 90;
    const MAX_VELOCITY: number = 400;
    const FLY_SPEED: number = 400;
    const TURN_SPEED: number = 150;
    const BANK_ANIMATION_RATE: number = 25;
    const BOUNCE: number = 0.1;

    const LEFT_EDGE_THRESHOLD: number = 10;
    const RIGHT_EDGE_THRESHOLD: number = 80;

    const MOUSE_WHEEL_BOOST_TIME: number = 200;

    const LASER_SPEED: number = 1200;
    const LASER_FIRE_RATE: number = 140;

    export class Player extends Phaser.Sprite {

        level1: Level1;

        body: Phaser.Physics.Arcade.Body;
        lastJumpTime: number;

        bankLeft: Phaser.Animation;
        bankRight: Phaser.Animation;

        thruster: Thruster;
        laserLeft: Laser;
        laserRight: Laser;
        mouseX: number;
        mouseY: number;

        boostToMouseOn: boolean = false;
        boostUntil: number;

        thrusterForceOn: boolean = false;
        thrusterForceOff: boolean = false;

        nextLaser: string = "left";

        constructor(game: Phaser.Game, x: number, y: number, thrusterType: string, level1: Level1) {

            super(game, x, y, 'goldship', 0);

            this.level1 = level1;

            this.anchor.setTo(0.5, 0.5);
            this.scale.set(0.5, 0.5);

            this.addAnimations();

            game.add.existing(this);
            game.physics.arcade.enable(this);

            this.body.drag.set(DRAG);
            this.body.maxVelocity.set(MAX_VELOCITY);
            this.angle = 90;
            this.body.bounce.set(BOUNCE);
            this.body.collideWorldBounds = true;

            var self = this;

            // Add thruster sprite
            this.thruster = new Thruster(this.game, 0, 0, thrusterType);

            this.addChild(this.thruster);

            // Add weapon
            this.armWeapons();

            document.onmousemove = function (e) {
                self.mouseX = e.screenX;
                self.mouseY = e.screenY;
            };

            document.onmousewheel = function (e) {
                self.boostUntil = Date.now() + MOUSE_WHEEL_BOOST_TIME;
            };

        }

        addAnimations(): void {
            this.animations.add('idle', [0], 1, false);
            this.animations.add('bankLeft', [1, 2, 3, 4], BANK_ANIMATION_RATE, false);
            this.animations.add('returnFromLeft', [4, 3, 2, 1], BANK_ANIMATION_RATE, false);
            this.animations.add('bankRight', [5, 6, 7, 8], BANK_ANIMATION_RATE, false);
            this.animations.add('returnFromRight', [8, 7, 6, 5], BANK_ANIMATION_RATE, false);
        }

        armWeapons(): void {
            this.laserLeft = this.game.add.weapon(30, 'laser');
            this.laserLeft.bulletKillType = Phaser.Weapon.KILL_CAMERA_BOUNDS;
            this.laserLeft.bulletSpeed = LASER_SPEED;
            this.laserLeft.fireRate = LASER_FIRE_RATE;
            this.laserLeft.trackSprite(this, -10, 31, true);
            this.laserLeft.bulletAngleOffset = 90;
            this.laserLeft.fireAngle = 90;

            /*this.laserRight = this.game.add.weapon(30, 'laser');
            this.laserRight.bulletKillType = Phaser.Weapon.KILL_CAMERA_BOUNDS;
            this.laserRight.bulletSpeed = LASER_SPEED;
            this.laserRight.fireRate = LASER_FIRE_RATE;
            this.laserRight.trackSprite(this, -10, -41, true);
            this.laserRight.bulletAngleOffset = 90;
            this.laserRight.fireAngle = 90;*/
        }

        mouseDownEvent(e): void {
            this.mouseX = e.screenX;
            this.mouseY = e.screenY;

            this.boostToMouseOn = true;
        }

        mouseUpEvent(e): void {
            this.boostToMouseOn = false;
        }

        update(): void {

            var goingLeftOrRight = 0;

            // Move towards mouse pointer/finger when click/tap
            if (this.game.input.activePointer.isDown || this.boostToMouseOn) {
                goingLeftOrRight = this.boostToMouse();
            }

            // Rotate when left/right pressed
            if (this.game.input.keyboard.isDown(Phaser.Keyboard.LEFT) || goingLeftOrRight == -1) {
                this.rotateLeft(goingLeftOrRight == -1);
            }
            else if (this.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT) || goingLeftOrRight == 1) {
                this.rotateRight(goingLeftOrRight == 1);
            } else {
                if (this.animations.currentAnim.name == "bankLeft") {
                    this.animations.play('returnFromLeft');
                } else if (this.animations.currentAnim.name == "bankRight") {
                    this.animations.play('returnFromRight');
                } else if ((this.animations.currentAnim.name == "returnFromLeft" || this.animations.currentAnim.name == "returnFromRight") && this.animations.currentAnim.isFinished) {
                    this.animations.play('idle');
                }

                this.body.angularVelocity = 0;
            }

            // Accelerate when up pressed
            if (this.game.input.keyboard.isDown(Phaser.Keyboard.UP) || this.boostToMouseOn || this.boostUntil > Date.now()) {
                this.turboBoostPower();
            } else {
                this.thruster.turnOffThruster();
                this.body.acceleration.set(0);
            }

            // Shoot when spacebar pressed
            if (this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
                /*if (this.nextLaser == "left") {
                    this.laserLeft.fire();
                    this.nextLaser = "right";
                } else {
                    this.laserRight.fire();
                    this.nextLaser = "left";
                }*/
                this.laserLeft.fire();

                if (this.laserLeft.trackOffset.y == -41) {
                    this.laserLeft.trackOffset.y = 31;
                } else {
                    this.laserLeft.trackOffset.y = -41;
                }
            }

            // Keep track of when keys are pressed (for tutorial purposes)
            if (this.game.input.keyboard.isDown(Phaser.Keyboard.UP) || this.game.input.keyboard.isDown(Phaser.Keyboard.LEFT) || this.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)) {
                this.level1.hasPressedArrows = true;
            }

            if (this.thrusterForceOn) {
                this.thruster.turnOnThruster();
            }

            if (this.thrusterForceOff) {
                this.thruster.turnOffThruster();
            }
        }

        boostToMouse(): number {
            this.turboBoostPower();

            var angleDiff = this.getAngleDiffWithPointer();

            if (angleDiff > 10) {
                //this.rotateRight();
                return 1;
            } else if (angleDiff < -10) {
                //this.rotateLeft();
                return -1;
            }

            return 0;
        }

        getShipAngle(): number {
            return this.angle;
        }

        getAngleDiffWithPointer(): number {
            var pointerAngle = this.game.physics.arcade.angleToXY(this.worldPosition, this.mouseX, this.mouseY) * 60;
            var shipAngle = this.getShipAngle();

            var angleDiff = pointerAngle - shipAngle;
            var finalDiff = angleDiff > 180 ? angleDiff - 360 : (angleDiff < -180 ? angleDiff + 360 : angleDiff);

            return finalDiff;
        }

        getShipRotation(): number {
            // TODO: try editing sprite file (rotate whole thing clockwise?) to avoid this mess

            return this.rotation;
            //return this.rotation - 1.5708;
        }

        rotateLeft(viaPointer: boolean): void {
            if (viaPointer) {
                var angleDiff = this.getAngleDiffWithPointer();
            }

            if (this.animations.currentAnim.name !== 'bankLeft' && (!viaPointer || angleDiff > 30 || angleDiff < -30)) {
                this.animations.play('bankLeft');
            }
            this.body.angularVelocity = -1 * TURN_SPEED;
        }

        rotateRight(viaPointer: boolean): void {
            if (viaPointer) {
                var angleDiff = this.getAngleDiffWithPointer();
            }

            if (this.animations.currentAnim.name !== 'bankRight' && (!viaPointer || angleDiff > 30 || angleDiff < -30)) {
                this.animations.play('bankRight');
            }
            this.body.angularVelocity = TURN_SPEED;
        }

        turboBoostPower(): void {
            this.thruster.turnOnThruster();
            this.game.physics.arcade.accelerationFromRotation(this.getShipRotation(), FLY_SPEED, this.body.acceleration);
        }
    }

}