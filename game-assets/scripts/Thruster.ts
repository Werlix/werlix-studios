﻿module SpaceshipGame {

    const ANIMATION_RATE: number = 25;

    export class Thruster extends Phaser.Sprite {

        timer: number;

        constructor(game: Phaser.Game, x: number, y: number, type: string) {

            if (type == 'gold') {
                super(game, x, y, 'goldthruster', 0);

                this.x -= 205;
                this.y -= 80;
            } else {
                super(game, x, y, 'bluethruster', 0);
                this.x -= 92;
                this.y += 25;
            }

            this.alpha = 0;

            this.animations.add('thrusterGo');
            this.animations.play('thrusterGo', ANIMATION_RATE, true);
        }

        turnOnThruster() {
            clearInterval(this.timer)
            this.timer = 0;

            this.alpha = 1;
        }

        turnOffThruster() {
            if (this.alpha > 0 && this.timer == 0) {
                var self = this;

                this.timer = setInterval(function () {
                    self.alpha -= 0.05;

                    if (self.alpha <= 0) {
                        clearInterval(self.timer);
                        self.timer = 0;
                    }
                }, 10);
            }
        }

        update() {

        }

    }
}