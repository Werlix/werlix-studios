<?php

global $project;
$project = 'mysite';

global $database;
$database = 'werlix10_werlixstudios';

if (substr($_SERVER['HTTP_HOST'], 0, 9) == 'localhost') {
    $database = 'silverstripe862';

    Security::setDefaultAdmin('admin', 'admin');
} else {
    Security::setDefaultAdmin('admin', 'Ws99tU%4Gf');
}

require_once('conf/ConfigureFromEnv.php');

// Set the site locale
i18n::set_locale('en_US');