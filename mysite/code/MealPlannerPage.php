<?php

require_once __DIR__ . '/library/shopify.php';

use phpish\shopify;

class MealPlannerPage extends Page
{
	private static $db = array(
	);

	private static $has_one = array(
	);

}
class MealPlannerPage_Controller extends Page_Controller
{
    private static $allowed_actions = array(
        'index',
        'oauth',
        'install',
        'getshop',
        'priv',
        'showMeals',
        'showMealPlans',
        'addMeal',
        'addMealPlan',
        'editMeal',
        'addMealToPlan'
	);
    /*
    public function priv(SS_HTTPRequest $request) {

        $this->response->addHeader('Content-Type', 'application/json');

        $shopify = shopify\client(AppSettings::SHOPIFY_SHOP, AppSettings::API_KEY, AppSettings::API_PASSWORD, true);

        $output = array();

        try
        {
            # Making an API request can throw an exception
            $products = $shopify('GET /admin/products.json', array('published_status'=>'published'));

            $this->response->addHeader('Content-Type', 'application/json');
            $output = $products;
        }
        catch (shopify\ApiException $e)
        {
            # HTTP status code was >= 400 or response contained the key 'errors'
            $output = $e;
        }
        catch (shopify\CurlException $e)
        {
            # cURL error
            $output = $e;
        }

        return json_encode($output);
    }*/

    public function index(SS_HTTPRequest $request) {
        $this->response->addHeader('Content-Type', 'application/json');

        $output = array();

        $shopQuery = $this->getRequest()->getVar('shop');

        if ($shopQuery) {
            $result = UserSettings::get()->filter('StoreName', $shopQuery)->limit(1);

            if ($result->exists()) {
                // Shop already exists
                // Check if its a valid request from Shopify
                if(shopify\is_valid_request($this->getGetParams(), AppSettings::SHARED_SECRET)) {
                    Session::set('shopify_signature', $_GET['signature']);
                    Session::set('shop', $shopQuery);

                    // Redirect to the admin page
                    header('Location: /meal-planner/admin');
                }

            } else {
                // Shop doesn't exist
                // Convert the permissions to an array
                $permissions = json_decode(AppSettings::PERMISSIONS, true);

                // Get the permission url
                $permission_url = shopify_api\permission_url(
                    $_GET['shop'], AppSettings::API_KEY, $permissions
                );
                $permission_url .= '&redirect_uri=' . $app_settings->redirect_url;

                header('Location: ' . $permission_url); //redirect to the permission url
            }

        }

        return json_encode($output);
    }

    public function oauth(SS_HTTPRequest $request) {
        # Guard: http://docs.shopify.com/api/authentication/oauth#verification
        shopify\is_valid_request($this->getGetParams(), AppSettings::SHARED_SECRET) or die('Invalid Request! Request or redirect did not come from Shopify');

        # Step 2: http://docs.shopify.com/api/authentication/oauth#asking-for-permission
        if (!isset($_GET['code']))
        {
            $permission_url = shopify\authorization_url($_GET['shop'], AppSettings::API_KEY, json_decode(AppSettings::PERMISSIONS, true), AppSettings::REDIRECT_URL);
            die("<script> top.location.href='$permission_url'</script>");
        }

        # Step 3: http://docs.shopify.com/api/authentication/oauth#confirming-installation
        try
        {
            # shopify\access_token can throw an exception
            $oauth_token = shopify\access_token($_GET['shop'], AppSettings::API_KEY, AppSettings::SHARED_SECRET, $_GET['code']);

            Session::set('oauth_token', $oauth_token);
            Session::set('shop', $_GET['shop']);

            header("Location: /meal-planner/show-meals");
            exit;
        }
        catch (shopify\ApiException $e)
        {
            # HTTP status code was >= 400 or response contained the key 'errors'
            echo $e;
            print_R($e->getRequest());
            print_R($e->getResponse());
        }
        catch (shopify\CurlException $e)
        {
            # cURL error
            echo $e;
            print_R($e->getRequest());
            print_R($e->getResponse());
        }
    }

    public function install(SS_HTTPRequest $request) {
        # Guard
        isset($_GET['shop']) or die ('Query parameter "shop" missing.');
        preg_match('/^[a-zA-Z0-9\-]+.myshopify.com$/', $_GET['shop']) or die('Invalid myshopify.com store URL.');

        $install_url = shopify\install_url($_GET['shop'], AppSettings::API_KEY);
        echo "<script> top.location.href='$install_url'</script>";
    }

    public function getshop(SS_HTTPRequest $request) {
        /*if (Session::get('shop')) {
            $shopName = Session::get('shop');
        } else {
            $shopName = $this->getRequest()->getVar('shop');

            Session::set('shop', $shopName);
        }*/

        $shopName = 'the-food-company.myshopify.com';

        if (!$shopName) {
            die("ERROR: Shop not found!");
        }

        $shopify = shopify\client($shopName, AppSettings::API_KEY, Session::get('oauth_token'));

        try
        {
            # Making an API request can throw an exception
            $shop = $shopify('GET /admin/shop.json');

            $this->response->addHeader('Content-Type', 'application/json');
            return json_encode($shop);
        }
        catch (shopify\ApiException $e)
        {
            # HTTP status code was >= 400 or response contained the key 'errors'
            echo $e;
            print_r($e->getRequest());
            print_r($e->getResponse());
        }
        catch (shopify\CurlException $e)
        {
            # cURL error
            echo $e;
            print_r($e->getRequest());
            print_r($e->getResponse());
        }
    }

    public function showMeals(SS_HTTPRequest $request) {
        $this->response->addHeader('Content-Type', 'application/json');

        $output = array();

        foreach (Meal::get() as $meal) {
            $output[] = $meal->getData();
        }

        return json_encode($output);
    }

    public function showMealPlans(SS_HTTPRequest $request) {
        $this->response->addHeader('Content-Type', 'application/json');

        $output = array();

        foreach (MealPlan::get() as $mealPlan) {
            $output[] = $mealPlan->getData();
        }

        return json_encode($output);
    }

    public function addMeal(SS_HTTPRequest $request) {
        $this->response->addHeader('Content-Type', 'application/json');

        $params = $this->getGetParams();

        $meal = new Meal();

        foreach (array('Name', 'Nutritionals', 'Image', 'Cost') as $key) {
            if (isset($params[$key])) {
                $meal->$key = $params[$key];
            } else {
                return json_encode("Error: $key param missing");
            }
        }

        $meal->DateAdded = date("Y-m-d");

        $meal->write();
        $meal->flushCache();

        return $meal->getAsJson();
    }

    public function editMeal(SS_HTTPRequest $request) {
        $this->response->addHeader('Content-Type', 'application/json');

        $params = $this->getGetParams();

        if (isset($params['ID'])) {
            $meal = Meal::get_by_id('Meal', $params['ID']);
        } else {
            return json_encode("Error: ID param missing");
        }

        if ($meal) {
            foreach (array('Name', 'Nutritionals', 'Image', 'Cost') as $key) {
                if (isset($params[$key])) {
                    $meal->$key = $params[$key];
                }
            }

            $meal->write();
            $meal->flushCache();

            return $meal->getAsJson();
        } else {
            return json_encode("Error: Meal not found");
        }
    }

    public function addMealPlan(SS_HTTPRequest $request) {
        $this->response->addHeader('Content-Type', 'application/json');

        $params = $this->getGetParams();

        $mealPlan = new MealPlan();

        foreach (array('DateStart') as $key) {
            if (isset($params[$key])) {
                $mealPlan->$key = $params[$key];
            } else {
                return json_encode("Error: $key param missing");
            }
        }

        $mealPlan->write();
        $mealPlan->flushCache();

        return $mealPlan->getAsJson();
    }

    public function addMealToPlan(SS_HTTPRequest $request) {
        $this->response->addHeader('Content-Type', 'application/json');

        $params = $this->getGetParams();

        if (isset($params['mealID'])) {
            if (isset($params['mealPlanID'])) {
                if (isset($params['day'])) {
                    if (in_array($params['day'], array('Mon', 'Tue', 'Wed', 'Thu', 'Fri'))) {
                        $meal = Meal::get_by_id('Meal', $params['mealID']);

                        if ($meal) {
                            $mealPlan = MealPlan::get_by_id('MealPlan', $params['mealPlanID']);

                            if ($mealPlan) {
                                $day = $params['day'] . "ID";
                                $dayMeals = $params['day'] . "Meals";

                                $mealPlan->$day = $meal->ID;
                                $meal->$dayMeals()->add($meal);

                                $mealPlan->write();
                                $mealPlan->flushCache();
                                $meal->write();
                                $meal->flushCache();

                                return $mealPlan->getAsJson();
                            } else {
                                return json_encode("Error: meal plan not found");
                            }
                        } else {
                            return json_encode("Error: meal not found");
                        }
                    } else {
                        return json_encode("Error: day not valid");
                    }
                } else {
                    return json_encode("Error: day param missing");
                }
            } else {
                return json_encode("Error: mealPlanID param missing");
            }
        } else {
            return json_encode("Error: mealID param missing");
        }
    }

    public function getGetParams() {
        $getParams = $this->getRequest()->getVars();

        unset($getParams['url']);

        return $getParams;
    }

}
