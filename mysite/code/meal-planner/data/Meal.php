<?php
class Meal extends DataObject
{
    private static $db = array(
        'Name' => 'Varchar(300)',
        'Nutritionals' => 'Text',
        'Image' => 'Varchar(300)',
        'Cost' => 'Float',
        'DateAdded' => 'Date'
    );

    private static $has_many = array(
        'MonMeals' => 'MealPlan',
        'TueMeals' => 'MealPlan',
        'WedMeals' => 'MealPlan',
        'ThuMeals' => 'MealPlan',
        'FriMeals' => 'MealPlan'
    );

    public function getData() {
        return array(
            'ID' => $this->ID,
            'Name' => $this->Name,
            'Nutritionals' => $this->Nutritionals,
            'Image' => $this->Image,
            'Cost' => $this->Cost,
            'DateAdded' => $this->DateAdded
        );
    }

    public function getAsJson() {
        return json_encode($this->getData());
    }
}