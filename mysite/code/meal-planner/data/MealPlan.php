<?php
class MealPlan extends DataObject
{
    private static $db = array(
        'DateStart' => 'Date'
    );

    private static $has_one = array(
        "Mon" => "Meal",
        "Tue" => "Meal",
        "Wed" => "Meal",
        "Thu" => "Meal",
        "Fri" => "Meal"
    );

    public function getData() {
        $output = array(
            'ID' => $this->ID,
            'DateStart' => $this->DateStart
        );

        if ($this->Mon) {
            $output['Mon'] = $this->Mon->getData();
        }

        if ($this->Tue) {
            $output['Tue'] = $this->Tue->getData();
        }

        if ($this->Wed) {
            $output['Wed'] = $this->Wed->getData();
        }

        if ($this->Thu) {
            $output['Thu'] = $this->Thu->getData();
        }

        if ($this->Fri) {
            $output['Fri'] = $this->Fri->getData();
        }

        return $output;
    }

    public function getAsJson() {
        return json_encode($this->getData());
    }
}