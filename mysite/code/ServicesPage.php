<?php
class ServicesPage extends Page {

	private static $db = array(
	);

	private static $has_one = array(
	);

}
class ServicesPage_Controller extends Page_Controller {

    public function index(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            return $this->renderWith("ServicesPage");
        }

        return $this;
    }

}
