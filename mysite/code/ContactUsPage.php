<?php
class ContactUsPage extends Page {

	private static $db = array(
	);

	private static $has_one = array(
	);

}
class ContactUsPage_Controller extends Page_Controller {

    private static $allowed_actions = array(
        'index',
        'form'
    );

    public function index(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            return $this->renderWith("ContactUsPage");
        }

        return $this;
    }

    public function form(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            $recaptchaResponse = $request->postVar('g-recaptcha-response');

            // verify recaptcha
            $url = "https://www.google.com/recaptcha/api/siteverify";
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 2);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "secret=6LdxKBYUAAAAABCsQz9irpAc8RT2_bS_Wy8avQgL&response=" . $recaptchaResponse);

            //execute post
            $result = curl_exec($ch);
            curl_close($ch);

            $json = json_decode($result, true);

            // Check if valid
            if (!isset($json['success']) || !$json['success']) {
                die('error');
            }

            $vars = $request->postVars();

            if (isset($vars['name']) && isset($vars['email']) && isset($vars['message'])) {
                if ($vars['name'] !== '' && $vars['email'] !== '' && $vars['message'] !== '') {
                    $email = new Email();
                    $email->setFrom('werlixstudios@gmail.com');
                    $email->setTo('werlixstudios@gmail.com');
                    $email->setSubject('Contact enquiry from werlixstudios.com');
                    $email->setBody("
                        <p><strong>You have a new enquiry from werlixstudios.com:</strong></p>
                        <p><strong>Name:</strong> {$vars['name']}</p>
                        <p><strong>Email:</strong> {$vars['email']}</p>
                        <p><strong>Message:</strong> <br/>" . nl2br($vars['message']) . "</p>
                    ");
                    $email->send();

                    die('success');
                }
            }
        }

        die('error');
    }
}
