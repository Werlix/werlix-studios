<?php

class ChronoscapePage extends Page
{
	private static $db = array(
	);

	private static $has_one = array(
	);

}
class ChronoscapePage_Controller extends Page_Controller
{
    private static $allowed_actions = array(
        'index',
        'log',
        'purge'
	);

    public function log(SS_HTTPRequest $request) {
        $this->response->addHeader('Content-Type', 'application/json');

        $output = array();

        $logObject = new ChronoscapeLog();

        foreach ($request->getVars() as $key => $val) {
            if ($key == "PlayerID") $logObject->PlayerID = $val;
            if ($key == "ChallengeID") $logObject->ChallengeID = $val;
            if ($key == "Message") $logObject->Message = $val;
            if ($key == "JSON") $logObject->JSON = $val;

            //$logObject->JSON = "{" . $key . ": " . $val . "}";
        }
        //$logObject->JSON = json_encode($request->getVars());
        $logObject->Datetime = date("Y-m-d H:i:s");

        $logObject->write();
        //$logObject->doRestoreToStage();
        $logObject->flushCache();

        return json_encode($output);
    }

    public function getAllLogs() {
        return ChronoscapeLog::get();
    }

    public function purge(SS_HTTPRequest $request) {
        $logs = ChronoscapeLog::get();

        foreach ($logs as $log) {
            //$log->deleteFromStage("Live");
            //$log->deleteFromStage("Stage");
            $log->delete();
        }
    }
}
