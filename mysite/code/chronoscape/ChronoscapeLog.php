<?php
class ChronoscapeLog extends DataObject
{
    private static $db = array(
        'PlayerID' => 'Varchar(300)',
        'ChallengeID' => 'Varchar(300)',
        'Message' => 'Varchar(300)',
        'Datetime' => 'SS_Datetime',
        'JSON' => 'Text'
    );

    public function getJsonDumped() {
        return "<pre>" . json_encode(json_decode($this->JSON), JSON_PRETTY_PRINT) . "</pre>";
    }
}