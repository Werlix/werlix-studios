<?php
class AboutPage extends Page {

	private static $db = array(
	);

	private static $has_one = array(
	);

}
class AboutPage_Controller extends Page_Controller {

    public $isAjax = false;

    public function index(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            $this->isAjax = true;
            return $this->renderWith("AboutPage");
        }

        return $this;
    }

    public function GetIsAjax() {
        return $this->isAjax;
    }
}
