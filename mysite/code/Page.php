<?php
class Page extends SiteTree {

	private static $db = array(
	);

	private static $has_one = array(
	);

}
class Page_Controller extends ContentController
{
    const SESSION_ONE_PAGE = 'onepage';

	/**
	 * An array of actions that can be accessed via a request. Each array element should be an action name, and the
	 * permissions or conditions required to allow the user to access it.
	 *
	 * <code>
	 * array (
	 *     'action', // anyone can access this action
	 *     'action' => true, // same as above
	 *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
	 *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
	 * );
	 * </code>
	 *
	 * @var array
	 */
	private static $allowed_actions = array (
	);

	public function init() {
		parent::init();

        // Change from one page to not one page
        if (Session::get(self::SESSION_ONE_PAGE) === null) {
            Session::set(self::SESSION_ONE_PAGE, true);
        }

        $onePage = $this->getRequest()->getVar('onepage');

        if ($onePage !== null) {
            Session::set(self::SESSION_ONE_PAGE, $onePage !== 'no');
        }

        // Include site CSS and JS
        $themeFolder = $this->ThemeDir();

        $CSS = array(
            $themeFolder . '/assets/bootstrap/css/bootstrap.min.css',
            $themeFolder . '/assets/ionicons/css/ionicons.min.css',
            $themeFolder . '/assets/bower_components/flexslider/flexslider.css',
            $themeFolder . '/assets/cubeportfolio/css/cubeportfolio.min.css',
            $themeFolder . '/assets/dzsparallaxer/dzsparallaxer.css'
        );

        Requirements::set_combined_files_folder($themeFolder . '/css');
        Requirements::combine_files("combined.css", $CSS);

        Requirements::css($themeFolder . '/assets/css/style.css');
        Requirements::css($themeFolder . '/css/site.min.css');

        Requirements::javascript($themeFolder . '/javascript/jquery.min.js');
        Requirements::javascript($themeFolder . '/assets/js/jquery-migrate.min.js');
        Requirements::javascript($themeFolder . '/assets/js/jquery.easing.min.js');
        Requirements::javascript($themeFolder . '/assets/bootstrap/js/bootstrap.min.js');

        /*$JS = array(
            $themeFolder . '/assets/bower_components/flexslider/jquery.flexslider-min.js',
            $themeFolder . '/assets/js/modernizr.custom.97074.js',
            $themeFolder . '/assets/js/jquery.sticky.js',
            $themeFolder . '/assets/bower_components/scrollreveal/dist/scrollreveal.min.js',
            $themeFolder . '/assets/dzsparallaxer/dzsparallaxer.js',
            $themeFolder . '/assets/js/template-custom.js',
            $themeFolder . '/assets/cubeportfolio/js/jquery.cubeportfolio.min.js',
            $themeFolder . '/assets/js/cube-awesome-work.js',
            $themeFolder . '/assets/js/newsletter-custom.js',
            $themeFolder . '/assets/js/scroll-reavel-custom.js'
        );

        Requirements::set_combined_files_folder($themeFolder . '/javascript');
        Requirements::combine_files("combined.js", $JS);*/

        //Requirements::javascript('http://maps.google.com/maps/api/js?sensor=false');
        //Requirements::javascript($themeFolder . '/assets/js/google-map.js');

        Requirements::javascript($themeFolder . '/javascript/script.js');
    }

    public function GetIsOnePage() {
        return !!Session::get(self::SESSION_ONE_PAGE);
    }
}
