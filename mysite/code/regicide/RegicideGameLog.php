<?php
class RegicideGameLog extends DataObject
{
    private static $db = array(
        'Datetime' => 'SS_Datetime',
        'Enemies' => 'Varchar(255)',
        'NumberOfPlayers' => 'Int',
        'HandLimit' => 'Int',
        'EnemiesDefeated' => 'Int',
        'Win' => 'Boolean',
        'Username' => 'Varchar(255)',
        'Version' => 'Int',
    );

    const CURRENT_VERSION = 2;
}