<?php

class RegicideLogPage extends Page
{
	private static $db = array(
	);

	private static $has_one = array(
	);

}
class RegicideLogPage_Controller extends Page_Controller
{
    private static $allowed_actions = array(
        'index',
        'log',
        'stats',
	);

    public function init() {
        $this->_stats = new stdClass();

        return parent::init();
    }

    public function stats(SS_HTTPRequest $request) {
        if ($request->requestVar('version')) {
            $this->_changeToVersion = $request->requestVar('version');
        }
    }

    public function log(SS_HTTPRequest $request) {
        $this->response->addHeader('Content-Type', 'application/json');

        $logObject = new RegicideGameLog();

        $logObject->Version = RegicideGameLog::CURRENT_VERSION;

        foreach ($request->requestVars() as $key => $val) {
            if ($key == "enemies") $logObject->Enemies = $val;
            if ($key == "numberOfPlayers") $logObject->NumberOfPlayers = $val;
            if ($key == "handLimit") $logObject->HandLimit = $val;
            if ($key == "win") $logObject->Win = $val;
            if ($key == "enemiesDefeated") $logObject->EnemiesDefeated = $val;
            if ($key == "username") $logObject->Username = $val;
        }

        if ($request->requestVar('numberOfPlayers') && $request->requestVar('handLimit')) {
            $logObject->Datetime = date("Y-m-d H:i:s");

            $logObject->write();
            //$logObject->doRestoreToStage();
            $logObject->flushCache();
        }

        return json_encode($logObject);
    }

    public function getAllRegicideLogs() {
        return RegicideGameLog::get();
    }

    public function getAllVersions() {
        return new ArrayList([1, 2]);
    }

    public function getCurrentVersion() {
        return RegicideGameLog::CURRENT_VERSION;
    }

    private $_stats;

    /** @var $_players RegicidePlayerStats[] */
    private $_players;

    private $_currentVersion = 0;
    private $_changeToVersion = RegicideGameLog::CURRENT_VERSION;

    private function _initStats() {
        $logs = RegicideGameLog::get();

        $this->_stats->fourPWins = 0;
        $this->_stats->threePWins = 0;
        $this->_stats->twoPWins = 0;
        $this->_stats->wins = 0;
        $this->_stats->fourPLosses = 0;
        $this->_stats->threePLosses = 0;
        $this->_stats->twoPLosses = 0;
        $this->_stats->losses = 0;
        $this->_stats->playerWins = [];
        $this->_stats->playerLosses = [];

        foreach ($logs as $log) {
            if ($log->Version != $this->_currentVersion) continue;

            if ($log->Username) {
                if (!isset($this->_players[$log->Username])) {
                    $this->_players[$log->Username] = new RegicidePlayerStats($log->Username);
                }

                if ($log->Win) {
                    $this->_players[$log->Username]->Wins++;
                } else {
                    $this->_players[$log->Username]->Losses++;
                }
            }

            if ($log->Win) {
                $this->_stats->wins++;
            } else {
                $this->_stats->losses++;
            }

            switch ($log->NumberOfPlayers) {
                case 4:
                    if ($log->Win) $this->_stats->fourPWins++;
                    else $this->_stats->fourPLosses++;
                    break;
                case 3:
                    if ($log->Win) $this->_stats->threePWins++;
                    else $this->_stats->threePLosses++;
                    break;
                case 2:
                    if ($log->Win) $this->_stats->twoPWins++;
                    else $this->_stats->twoPLosses++;
                    break;
            }
        }
    }

    public function getStats() {
        if ($this->_currentVersion != $this->_changeToVersion) {
            $this->_currentVersion = $this->_changeToVersion;
            $this->_stats = $this->_initStats();
        }

        return $this->_stats;
    }

    public function getStat($statName) {
        return $this->getStats()->$statName;
    }

    public function getPlayerWins($playerName) {
        return isset($this->getStats()->playerWins[$playerName]) ? $this->getStats()->playerWins[$playerName] : 0;
    }

    public function getPlayerLosses($playerName) {
        return isset($this->getStats()->playerLosses[$playerName]) ? $this->getStats()->playerLosses[$playerName] : 0;
    }

    public function getPlayerWinPercentage($playerName) {
        return number_format(($this->getPlayerWins($playerName) / ($this->getPlayerWins($playerName) + $this->getPlayerLosses($playerName)) * 100), 1);
    }

    public function getPercentage($stat1, $stat2) {
        return number_format(($this->getStats()->$stat1 / ($this->getStats()->$stat1 + $this->getStats()->$stat2) * 100), 1);
    }

    public function getPlayers() {
        return new ArrayList($this->_players);
    }
}

class RegicidePlayerStats extends ViewableData
{
    public $Username;
    public $Wins = 0;
    public $Losses = 0;

    public function __construct($username) {
        $this->Username = $username;
    }

    public function winPercentage() {
        return number_format(($this->Wins / ($this->Wins + $this->Losses) * 100), 1);
    }
}