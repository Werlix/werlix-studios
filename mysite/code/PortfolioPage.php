<?php
class PortfolioPage extends Page {

	private static $db = array(
	);

	private static $has_one = array(
	);

}
class PortfolioPage_Controller extends Page_Controller {

    public function index(SS_HTTPRequest $request) {
        if ($request->isAjax()) {
            return $this->renderWith("PortFolioPage");
        }

        return $this;
    }

}
