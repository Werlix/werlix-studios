var game;
window.onload = function () {
    game = new SpaceshipGame.Game();
};
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var SpaceshipGame;
(function (SpaceshipGame) {
    var Boot = (function (_super) {
        __extends(Boot, _super);
        function Boot() {
            _super.apply(this, arguments);
        }
        Boot.prototype.preload = function () {
            window.scrollTo(0, 0);
            this.load.image('preloadBar', 'game-assets/loader.png');
        };
        Boot.prototype.create = function () {
            //  Unless you specifically need to support multitouch I would recommend setting this to 1
            this.input.maxPointers = 1;
            //  Phaser will automatically pause if the browser tab the game is in loses focus. You can disable that here:
            this.stage.disableVisibilityChange = true;
            if (this.game.device.desktop) {
            }
            else {
            }
            this.scale.scaleMode = Phaser.ScaleManager.RESIZE;
            this.game.state.start('Preloader', true, false);
            //this.game.stage.backgroundColor = '#5555ff';
            //this.game.stage.worldAlpha = 0.4;
            //this.game.state.onResizeCallback = this.pageResized;
            //window.onresize = this.pageResized;
        };
        Boot.prototype.pageResized = function (x, y) {
            //this.game.world.setBounds(0, 0, x, 5000);
        };
        return Boot;
    }(Phaser.State));
    SpaceshipGame.Boot = Boot;
})(SpaceshipGame || (SpaceshipGame = {}));
var SpaceshipGame;
(function (SpaceshipGame) {
    var Game = (function (_super) {
        __extends(Game, _super);
        function Game() {
            _super.call(this, '100%', '100%', Phaser.AUTO, 'game-content', null, true);
            this.setPageHeight();
            this.state.add('Boot', SpaceshipGame.Boot, false);
            this.state.add('Preloader', SpaceshipGame.Preloader, false);
            this.state.add('MainMenu', SpaceshipGame.MainMenu, false);
            this.state.add('Level1', SpaceshipGame.Level1, false);
            this.state.start('Boot');
        }
        Game.prototype.getPageHeight = function () {
            return this.pageHeight;
        };
        Game.prototype.setPageHeight = function () {
            var body = document.body;
            var html = document.documentElement;
            this.pageHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
        };
        return Game;
    }(Phaser.Game));
    SpaceshipGame.Game = Game;
})(SpaceshipGame || (SpaceshipGame = {}));
var SpaceshipGame;
(function (SpaceshipGame) {
    var Laser = (function (_super) {
        __extends(Laser, _super);
        function Laser(game, parent) {
            _super.call(this, game, parent);
        }
        return Laser;
    }(Phaser.Weapon));
    SpaceshipGame.Laser = Laser;
})(SpaceshipGame || (SpaceshipGame = {}));
var SpaceshipGame;
(function (SpaceshipGame) {
    var Level1 = (function (_super) {
        __extends(Level1, _super);
        function Level1() {
            _super.apply(this, arguments);
            this.player = null;
            this.goingToY = -1;
            this.scrollingToY = -1;
            this.hasClicked = false;
            this.hasPressedArrows = false;
        }
        Level1.prototype.create = function () {
            this.game.world.setBounds(0, 0, this.game.width, this.game.pageHeight);
            //this.player = new Player(this.game, this.game.world.centerX - 100, 300, 'blue');
            var self = this;
            window.addEventListener('resize', function () {
                // Set game to match window width
                var pageWidth = self.getPageWidth();
                self.game.width = pageWidth;
                self.game.canvas.width = pageWidth;
                self.game.world.setBounds(0, 0, pageWidth, self.game.pageHeight);
                self.game.world.x = 0;
            });
            document.onmousedown = function (e) {
                console.log('onmousedown');
                if (self.player !== null) {
                    self.hasClicked = true;
                    self.player.mouseDownEvent(e);
                }
            };
            document.onmouseup = function (e) {
                console.log('onmouseup');
                if (self.player !== null) {
                    self.player.mouseUpEvent(e);
                }
            };
            document.ontouchstart = function (e) {
                console.log('ontouchstart');
                if (self.player !== null) {
                    self.hasClicked = true;
                    self.player.mouseDownEvent(e);
                }
            };
            document.ontouchend = function (e) {
                console.log('ontouchend');
                if (self.player !== null) {
                    self.player.mouseUpEvent(e);
                }
            };
            // When "goingTo" links are clicked, scroll to that element
            var scrollLinks = document.getElementsByClassName('goingTo');
            for (var i = 0; i < scrollLinks.length; i++) {
                scrollLinks[i].addEventListener('click', function (e) {
                    var target = this.getAttribute('data-target');
                    if (target.substr(0, 1) == '#') {
                        target = target.substr(1);
                    }
                    var targetElement = document.getElementById(target);
                    if (targetElement) {
                        if (self.player !== null) {
                            self.goingToY = targetElement.offsetTop + (self.game.height / 2);
                            if (self.goingToY < self.player.y) {
                                self.player.angle = -90;
                            }
                            else {
                                self.player.angle = 90;
                            }
                            self.player.thrusterForceOn = true;
                            self.player.body.acceleration.set(0);
                            self.player.body.velocity.x = 0;
                            self.player.body.velocity.y = 0;
                        }
                        else {
                            self.scrollingToY = targetElement.offsetTop;
                        }
                        e.preventDefault();
                        return false;
                    }
                }, false);
            }
            var beginGameButtons = document.getElementsByClassName('beginGame');
            var beginGameListener = function (e) {
                if (self.player == null) {
                    self.game.setPageHeight();
                    self.game.world.setBounds(0, 0, self.game.width, self.game.pageHeight);
                    self.player = new SpaceshipGame.Player(self.game, self.game.world.centerX, document.body.scrollTop + (self.game.height / 2), 'gold', self);
                    self.game.camera.follow(self.player);
                }
                self.startTutorial();
                self.addGameCSS();
                var beginGame1 = document.getElementById('beginGame1');
                var beginGame2 = document.getElementById('beginGame2');
                if (beginGame1) {
                    beginGame1.parentNode.removeChild(beginGame1);
                }
                if (beginGame2) {
                    beginGame2.parentNode.removeChild(beginGame2);
                }
            };
            for (var i = 0; i < beginGameButtons.length; i++) {
                beginGameButtons[i].addEventListener('click', beginGameListener);
            }
        };
        Level1.prototype.update = function () {
            if (this.goingToY > -1) {
                if ((this.player.y + 25) < this.goingToY) {
                    this.player.y += 25;
                }
                else if ((this.player.y - 25) > this.goingToY) {
                    this.player.y -= 25;
                }
                else {
                    this.goingToY = -1;
                    this.player.thrusterForceOn = false;
                    this.player.thrusterForceOff = true;
                }
            }
            if (this.scrollingToY > -1) {
                //console.log('Scrolling to: ' + this.scrollingToY + ' current scroll: ' + document.documentElement.scrollTop);
                if ((document.documentElement.scrollTop + 40) < this.scrollingToY) {
                    //document.documentElement.scrollTop += 40;
                    window.scrollTo(0, document.documentElement.scrollTop + 40);
                }
                else if ((document.documentElement.scrollTop - 40) > this.scrollingToY) {
                    //document.documentElement.scrollTop -= 40;
                    window.scrollTo(0, document.documentElement.scrollTop - 40);
                }
                else {
                    this.scrollingToY = -1;
                }
            }
            if (this.player !== null) {
                window.scrollTo(0, this.game.camera.y);
            }
        };
        Level1.prototype.startTutorial = function () {
            var self = this;
            setTimeout(function () {
                if (!self.hasClicked || !self.hasPressedArrows) {
                    document.getElementById('gameHint').style.transform = 'scale(1) translateX(-50%) translateY(-200%)';
                    document.getElementById('gameHint').style.display = 'block';
                }
                var interval = setInterval(function () {
                    if (self.hasClicked || self.hasPressedArrows) {
                        document.getElementById('gameHint').style.transform = 'scale(0) translateX(-50%) translateY(-200%)';
                        document.getElementById('gameHint').style.display = 'hidden';
                        clearInterval(interval);
                    }
                }, 3000);
                setTimeout(function () {
                    document.getElementById('gameHint').style.transform = 'scale(0) translateX(-50%) translateY(-200%)';
                    document.getElementById('gameHint').style.display = 'hidden';
                    clearInterval(interval);
                }, 8000);
            }, 6000);
        };
        Level1.prototype.addGameCSS = function () {
            document.body.style.cssText =
                "overflow: hidden;" +
                    "-webkit-touch-callout: none;" +
                    "-webkit-user-select: none;" +
                    "-khtml-user-select: none;" +
                    "-moz-user-select: none;" +
                    "-ms-user-select: none;" +
                    "user-select: none;" +
                    "cursor: url('/game-assets/cursor.cur'), auto;";
        };
        Level1.prototype.getPageWidth = function () {
            var body = document.body;
            var html = document.documentElement;
            return Math.max(body.scrollWidth, body.offsetWidth, html.clientWidth, html.scrollWidth, html.offsetWidth);
        };
        Level1.prototype.getCoords = function (elem) {
            var box = elem.getBoundingClientRect();
            var body = document.body;
            var docEl = document.documentElement;
            var scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
            var scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft;
            var clientTop = docEl.clientTop || body.clientTop || 0;
            var clientLeft = docEl.clientLeft || body.clientLeft || 0;
            var top = box.top + scrollTop - clientTop;
            var left = box.left + scrollLeft - clientLeft;
            return new Phaser.Point(left, top);
        };
        return Level1;
    }(Phaser.State));
    SpaceshipGame.Level1 = Level1;
})(SpaceshipGame || (SpaceshipGame = {}));
var SpaceshipGame;
(function (SpaceshipGame) {
    var MainMenu = (function (_super) {
        __extends(MainMenu, _super);
        function MainMenu() {
            _super.apply(this, arguments);
        }
        MainMenu.prototype.create = function () {
            this.background = this.add.sprite(0, 0, 'titlepage');
            this.background.alpha = 0;
            this.logo = this.add.sprite(this.world.centerX, -300, 'logo');
            this.logo.anchor.setTo(0.5, 0.5);
            this.add.tween(this.background).to({ alpha: 1 }, 2000, Phaser.Easing.Bounce.InOut, true);
            this.add.tween(this.logo).to({ y: 220 }, 2000, Phaser.Easing.Elastic.Out, true, 2000);
            this.input.onDown.addOnce(this.fadeOut, this);
        };
        MainMenu.prototype.fadeOut = function () {
            this.add.tween(this.background).to({ alpha: 0 }, 2000, Phaser.Easing.Linear.None, true);
            var tween = this.add.tween(this.logo).to({ y: 800 }, 2000, Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.startGame, this);
        };
        MainMenu.prototype.startGame = function () {
            this.game.state.start('Level1', true, false);
        };
        return MainMenu;
    }(Phaser.State));
    SpaceshipGame.MainMenu = MainMenu;
})(SpaceshipGame || (SpaceshipGame = {}));
var SpaceshipGame;
(function (SpaceshipGame) {
    var DRAG = 90;
    var MAX_VELOCITY = 400;
    var FLY_SPEED = 400;
    var TURN_SPEED = 150;
    var BANK_ANIMATION_RATE = 25;
    var BOUNCE = 0.1;
    var LEFT_EDGE_THRESHOLD = 10;
    var RIGHT_EDGE_THRESHOLD = 80;
    var MOUSE_WHEEL_BOOST_TIME = 200;
    var LASER_SPEED = 1200;
    var LASER_FIRE_RATE = 140;
    var Player = (function (_super) {
        __extends(Player, _super);
        function Player(game, x, y, thrusterType, level1) {
            _super.call(this, game, x, y, 'goldship', 0);
            this.boostToMouseOn = false;
            this.thrusterForceOn = false;
            this.thrusterForceOff = false;
            this.nextLaser = "left";
            this.level1 = level1;
            this.anchor.setTo(0.5, 0.5);
            this.scale.set(0.5, 0.5);
            this.addAnimations();
            game.add.existing(this);
            game.physics.arcade.enable(this);
            this.body.drag.set(DRAG);
            this.body.maxVelocity.set(MAX_VELOCITY);
            this.angle = 90;
            this.body.bounce.set(BOUNCE);
            this.body.collideWorldBounds = true;
            var self = this;
            // Add thruster sprite
            this.thruster = new SpaceshipGame.Thruster(this.game, 0, 0, thrusterType);
            this.addChild(this.thruster);
            // Add weapon
            this.armWeapons();
            document.onmousemove = function (e) {
                self.mouseX = e.screenX;
                self.mouseY = e.screenY;
            };
            document.onmousewheel = function (e) {
                self.boostUntil = Date.now() + MOUSE_WHEEL_BOOST_TIME;
            };
        }
        Player.prototype.addAnimations = function () {
            this.animations.add('idle', [0], 1, false);
            this.animations.add('bankLeft', [1, 2, 3, 4], BANK_ANIMATION_RATE, false);
            this.animations.add('returnFromLeft', [4, 3, 2, 1], BANK_ANIMATION_RATE, false);
            this.animations.add('bankRight', [5, 6, 7, 8], BANK_ANIMATION_RATE, false);
            this.animations.add('returnFromRight', [8, 7, 6, 5], BANK_ANIMATION_RATE, false);
        };
        Player.prototype.armWeapons = function () {
            this.laserLeft = this.game.add.weapon(30, 'laser');
            this.laserLeft.bulletKillType = Phaser.Weapon.KILL_CAMERA_BOUNDS;
            this.laserLeft.bulletSpeed = LASER_SPEED;
            this.laserLeft.fireRate = LASER_FIRE_RATE;
            this.laserLeft.trackSprite(this, -10, 31, true);
            this.laserLeft.bulletAngleOffset = 90;
            this.laserLeft.fireAngle = 90;
            /*this.laserRight = this.game.add.weapon(30, 'laser');
            this.laserRight.bulletKillType = Phaser.Weapon.KILL_CAMERA_BOUNDS;
            this.laserRight.bulletSpeed = LASER_SPEED;
            this.laserRight.fireRate = LASER_FIRE_RATE;
            this.laserRight.trackSprite(this, -10, -41, true);
            this.laserRight.bulletAngleOffset = 90;
            this.laserRight.fireAngle = 90;*/
        };
        Player.prototype.mouseDownEvent = function (e) {
            this.mouseX = e.screenX;
            this.mouseY = e.screenY;
            console.log('mouse down ' + this.mouseX + ',' + this.mouseY);
            this.boostToMouseOn = true;
        };
        Player.prototype.mouseUpEvent = function (e) {
            this.boostToMouseOn = false;
            console.log('mouse up');
        };
        Player.prototype.update = function () {
            var goingLeftOrRight = 0;
            // Move towards mouse pointer/finger when click/tap
            if (this.game.input.activePointer.isDown || this.boostToMouseOn) {
                goingLeftOrRight = this.boostToMouse();
            }
            // Rotate when left/right pressed
            if (this.game.input.keyboard.isDown(Phaser.Keyboard.LEFT) || goingLeftOrRight == -1) {
                this.rotateLeft(goingLeftOrRight == -1);
            }
            else if (this.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT) || goingLeftOrRight == 1) {
                this.rotateRight(goingLeftOrRight == 1);
            }
            else {
                if (this.animations.currentAnim.name == "bankLeft") {
                    this.animations.play('returnFromLeft');
                }
                else if (this.animations.currentAnim.name == "bankRight") {
                    this.animations.play('returnFromRight');
                }
                else if ((this.animations.currentAnim.name == "returnFromLeft" || this.animations.currentAnim.name == "returnFromRight") && this.animations.currentAnim.isFinished) {
                    this.animations.play('idle');
                }
                this.body.angularVelocity = 0;
            }
            // Accelerate when up pressed
            if (this.game.input.keyboard.isDown(Phaser.Keyboard.UP) || this.boostToMouseOn || this.boostUntil > Date.now()) {
                this.turboBoostPower();
            }
            else {
                this.thruster.turnOffThruster();
                this.body.acceleration.set(0);
            }
            // Shoot when spacebar pressed
            if (this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
                /*if (this.nextLaser == "left") {
                    this.laserLeft.fire();
                    this.nextLaser = "right";
                } else {
                    this.laserRight.fire();
                    this.nextLaser = "left";
                }*/
                this.laserLeft.fire();
                if (this.laserLeft.trackOffset.y == -41) {
                    this.laserLeft.trackOffset.y = 31;
                }
                else {
                    this.laserLeft.trackOffset.y = -41;
                }
            }
            // Keep track of when keys are pressed (for tutorial purposes)
            if (this.game.input.keyboard.isDown(Phaser.Keyboard.UP) || this.game.input.keyboard.isDown(Phaser.Keyboard.LEFT) || this.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)) {
                this.level1.hasPressedArrows = true;
            }
            if (this.thrusterForceOn) {
                this.thruster.turnOnThruster();
            }
            if (this.thrusterForceOff) {
                this.thruster.turnOffThruster();
            }
        };
        Player.prototype.boostToMouse = function () {
            this.turboBoostPower();
            var angleDiff = this.getAngleDiffWithPointer();
            if (angleDiff > 10) {
                //this.rotateRight();
                return 1;
            }
            else if (angleDiff < -10) {
                //this.rotateLeft();
                return -1;
            }
            return 0;
        };
        Player.prototype.getShipAngle = function () {
            return this.angle;
        };
        Player.prototype.getAngleDiffWithPointer = function () {
            var pointerAngle = this.game.physics.arcade.angleToXY(this.worldPosition, this.mouseX, this.mouseY) * 60;
            var shipAngle = this.getShipAngle();
            var angleDiff = pointerAngle - shipAngle;
            var finalDiff = angleDiff > 180 ? angleDiff - 360 : (angleDiff < -180 ? angleDiff + 360 : angleDiff);
            return finalDiff;
        };
        Player.prototype.getShipRotation = function () {
            // TODO: try editing sprite file (rotate whole thing clockwise?) to avoid this mess
            return this.rotation;
            //return this.rotation - 1.5708;
        };
        Player.prototype.rotateLeft = function (viaPointer) {
            if (viaPointer) {
                var angleDiff = this.getAngleDiffWithPointer();
            }
            if (this.animations.currentAnim.name !== 'bankLeft' && (!viaPointer || angleDiff > 30 || angleDiff < -30)) {
                this.animations.play('bankLeft');
            }
            this.body.angularVelocity = -1 * TURN_SPEED;
        };
        Player.prototype.rotateRight = function (viaPointer) {
            if (viaPointer) {
                var angleDiff = this.getAngleDiffWithPointer();
            }
            if (this.animations.currentAnim.name !== 'bankRight' && (!viaPointer || angleDiff > 30 || angleDiff < -30)) {
                this.animations.play('bankRight');
            }
            this.body.angularVelocity = TURN_SPEED;
        };
        Player.prototype.turboBoostPower = function () {
            this.thruster.turnOnThruster();
            this.game.physics.arcade.accelerationFromRotation(this.getShipRotation(), FLY_SPEED, this.body.acceleration);
        };
        return Player;
    }(Phaser.Sprite));
    SpaceshipGame.Player = Player;
})(SpaceshipGame || (SpaceshipGame = {}));
var SpaceshipGame;
(function (SpaceshipGame) {
    var Preloader = (function (_super) {
        __extends(Preloader, _super);
        function Preloader() {
            _super.apply(this, arguments);
        }
        Preloader.prototype.preload = function () {
            //  Set-up our preloader sprite
            //this.preloadBar = this.add.sprite(200, 250, 'preloadBar');
            //this.load.setPreloadSprite(this.preloadBar);
            //  Load our actual games assets
            this.load.spritesheet('goldship', 'game-assets/goldship.png', 212, 164, 9);
            this.load.spritesheet('purpleship', 'game-assets/purpleship.png', 164, 212, 9);
            this.load.spritesheet('goldthruster', 'game-assets/thrustergold.png', 181, 153);
            this.load.spritesheet('bluethruster', 'game-assets/thrusterbluesm.png', 181, 181);
            this.load.spritesheet('laser', 'game-assets/laser2.png', 17, 33);
        };
        Preloader.prototype.create = function () {
            //var tween = this.add.tween(this.preloadBar).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true);
            //tween.onComplete.add(this.startGame, this);
            //document.getElementById('preloader').remove();
            console.log('starting');
            this.startGame();
        };
        Preloader.prototype.startMainMenu = function () {
            this.game.state.start('MainMenu', true, false);
        };
        Preloader.prototype.startGame = function () {
            this.game.state.start('Level1', true, false);
        };
        return Preloader;
    }(Phaser.State));
    SpaceshipGame.Preloader = Preloader;
})(SpaceshipGame || (SpaceshipGame = {}));
var SpaceshipGame;
(function (SpaceshipGame) {
    var ANIMATION_RATE = 25;
    var Thruster = (function (_super) {
        __extends(Thruster, _super);
        function Thruster(game, x, y, type) {
            if (type == 'gold') {
                _super.call(this, game, x, y, 'goldthruster', 0);
                this.x -= 205;
                this.y -= 80;
            }
            else {
                _super.call(this, game, x, y, 'bluethruster', 0);
                this.x -= 92;
                this.y += 25;
            }
            this.alpha = 0;
            this.animations.add('thrusterGo');
            this.animations.play('thrusterGo', ANIMATION_RATE, true);
        }
        Thruster.prototype.turnOnThruster = function () {
            clearInterval(this.timer);
            this.timer = 0;
            this.alpha = 1;
        };
        Thruster.prototype.turnOffThruster = function () {
            if (this.alpha > 0 && this.timer == 0) {
                var self = this;
                this.timer = setInterval(function () {
                    self.alpha -= 0.05;
                    if (self.alpha <= 0) {
                        clearInterval(self.timer);
                        self.timer = 0;
                    }
                }, 10);
            }
        };
        Thruster.prototype.update = function () {
        };
        return Thruster;
    }(Phaser.Sprite));
    SpaceshipGame.Thruster = Thruster;
})(SpaceshipGame || (SpaceshipGame = {}));
//# sourceMappingURL=game.js.map